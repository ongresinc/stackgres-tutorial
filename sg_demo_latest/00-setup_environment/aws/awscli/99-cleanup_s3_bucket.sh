#!/bin/bash
parent_path=$( cd "$(dirname "${BASH_SOURCE[0]}")" ; pwd -P )

cd "$parent_path"
source ./variables

{
	aws iam delete-user-policy --region $AWS_REGION --user-name $S3_BACKUP_BUCKET_USER \
		--policy-name $S3_BACKUP_BUCKET_POLICY_NAME
	
	aws --region $AWS_REGION iam list-access-keys --user-name $S3_BACKUP_BUCKET_USER \
		| jq -r '.AccessKeyMetadata[].AccessKeyId' \
		| xargs -r -n 1 -I % \
		aws --region $AWS_REGION iam delete-access-key \
		--user-name $S3_BACKUP_BUCKET_USER --access-key-id %
	
	aws iam delete-user --region $AWS_REGION --user-name $S3_BACKUP_BUCKET_USER

	aws s3 rb s3://$S3_BACKUP_BUCKET --region $AWS_REGION --force
} &> /dev/null



