#!/bin/bash
parent_path=$( cd "$(dirname "${BASH_SOURCE[0]}")" ; pwd -P )

cd "$parent_path"
source ./variables

eksctl --region $AWS_REGION create cluster --name $K8S_CLUSTER_NAME \
	--node-type m5a.2xlarge --node-volume-size 100 --nodes 3 \
	--zones ${AWS_REGION}a,${AWS_REGION}b,${AWS_REGION}c \
	--version 1.17
